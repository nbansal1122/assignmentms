package ms.assignmentms.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ms.assignmentms.R;
import ms.assignmentms.models.Page;

/**
 * Created by nbansal2211 on 06/06/16.
 */
public class PageAdapter extends ArrayAdapter<Page> {

    private List<Page> pages;
    private int resourceId;
    private Picasso picasso;

    public PageAdapter(Context context, int resource, List<Page> pages) {
        super(context, resource, pages);
        this.resourceId = resource;
        this.pages = pages;
        picasso = Picasso.with(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(resourceId, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Page page = pages.get(position);
        if (!TextUtils.isEmpty(page.getThumbnail().getSource())) {
            picasso.load(page.getThumbnail().getSource()).fit().into(holder.imageView);
        }
        if (holder.title != null && !TextUtils.isEmpty(page.getTitle())) {
            holder.title.setText(page.getTitle());
        }
        return convertView;
    }

    private class ViewHolder {
        ImageView imageView;
        TextView title;

        public ViewHolder(View view) {
            imageView = (ImageView) view.findViewById(R.id.iv_thumbnail);
            title = (TextView) view.findViewById(R.id.tv_pageName);
        }
    }
}
