package ms.assignmentms.interfaces;

import java.util.List;

import ms.assignmentms.models.Page;

/**
 * Created by nbansal2211 on 06/06/16.
 */
public interface AsyncInterface {

    public void onPreExecute(String searchString);
    public void onPostExecute(String searchTerm, List<Page> pages);

}
