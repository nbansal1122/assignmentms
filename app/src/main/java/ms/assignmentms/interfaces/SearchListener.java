package ms.assignmentms.interfaces;

import java.util.List;

import ms.assignmentms.models.Page;

/**
 * Created by nbansal2211 on 07/06/16.
 */
public interface SearchListener {
    public void updateSearchResults(String searchTerm, List<Page> pages);
}
