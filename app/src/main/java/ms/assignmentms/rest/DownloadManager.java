package ms.assignmentms.rest;

import android.os.AsyncTask;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import ms.assignmentms.interfaces.AsyncInterface;
import ms.assignmentms.models.Page;
import ms.assignmentms.util.AppConstants;
import ms.assignmentms.util.JsonParserUtil;

/**
 * Created by nbansal2211 on 06/06/16.
 */
public class DownloadManager extends AsyncTask<Void, Void, List<Page>> {
    private AsyncInterface listener;
    private String searchTerm;

    public DownloadManager(AsyncInterface listener, String searchTerm) {
        this.listener = listener;
        this.searchTerm = searchTerm;
    }

    @Override
    protected void onPreExecute() {
        listener.onPreExecute(searchTerm);
    }

    @Override
    protected List<Page> doInBackground(Void... params) {
        List<Page> pages = new ArrayList<>();
        try {
            String url = String.format(AppConstants.BASE_URL, URLEncoder.encode(searchTerm, "UTF-8"));
            String jsonString = new RestClient().getJSONFromServer(url);
            pages = JsonParserUtil.getPageList(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return pages;
    }

    @Override
    protected void onPostExecute(List<Page> pages) {
        if (listener != null) {
            listener.onPostExecute(searchTerm, pages);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
