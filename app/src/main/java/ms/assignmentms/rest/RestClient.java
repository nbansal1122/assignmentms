package ms.assignmentms.rest;

import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;


public class RestClient {
    private static final int HTTP_RESPONSE_OK = 200;

    private InputStream OpenHttpConnection(String url) throws IOException {
        Log.d("palval", "OpenHttpConnection");
        InputStream in = null;
        URLConnection conn = new URL(url).openConnection();
        if (conn instanceof HttpURLConnection) {
            try {
                HttpURLConnection httpConn = (HttpURLConnection) conn;
                httpConn.setRequestMethod("GET");
                httpConn.setRequestProperty("Content-Type", "application/json");

                httpConn.connect();
                int response = httpConn.getResponseCode();
                if (response == HTTP_RESPONSE_OK) {
                    in = httpConn.getInputStream();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Exception", "Unable To Connect");
            }
            Log.d("RestClient", "Returning stream");
            return in;
        }
        throw new IOException("Not an HTTP connection");
    }

    public String getJSONFromServer(String url) {
        InputStream in = null;
        String json = "";
        try {
            in = OpenHttpConnection(url);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                sb.append(line + "\n");
            }
            in.close();
            json = sb.toString();
            Log.d("Response Received :", json + "");
        } catch (Exception e4) {
            Log.e("Buffer Error", "Error converting result " + e4.toString());
        }
        return json;
    }
}
