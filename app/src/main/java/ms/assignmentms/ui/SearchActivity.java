package ms.assignmentms.ui;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import ms.assignmentms.R;
import ms.assignmentms.interfaces.AsyncInterface;
import ms.assignmentms.interfaces.SearchListener;
import ms.assignmentms.models.Page;
import ms.assignmentms.rest.DownloadManager;
import ms.assignmentms.ui.fragments.SearchGridFragment;
import ms.assignmentms.ui.fragments.SearchListFragment;
import ms.assignmentms.util.Util;

public class SearchActivity extends AppCompatActivity implements AsyncInterface, View.OnClickListener {

    private static final int SEARCH_DELAY = 500;
    private static final int MIN_CHARACTER_LENGTH = 3;
    private static final int MESSAGE_TEXT_CHANGED = 1;
    private SearchListener searchFragment;
//    private SearchListFragment searchFragment;
        private static final String TAG = SearchActivity.class.getSimpleName();
    private DownloadManager downloadManager;
    private ProgressBar progressBar;
    private ImageView searchImage;
    private EditText searchBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressBar = (ProgressBar) findViewById(R.id.pb_loading_indicator);
        searchImage = (ImageView) findViewById(R.id.search_magnifying_glass);

        searchImage.setOnClickListener(this);

        searchFragment = new SearchListFragment();
        getFragmentManager().beginTransaction().replace(R.id.frame, (Fragment) searchFragment).commit();

        searchBox = (EditText) findViewById(R.id.et_search_box);
        searchBox.setHint(R.string.hint_type);
        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchTerm = s.toString().trim();
                if (searchTerm.length() >= MIN_CHARACTER_LENGTH) {
                    sendSearchMessage(searchTerm);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d(TAG, "After Text Changed");
            }
        });

    }


    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            startSearch((String) msg.obj);
        }
    };


    private void sendSearchMessage(String searchTerm) {
        handler.removeMessages(MESSAGE_TEXT_CHANGED);
        handler.sendMessageDelayed(handler.obtainMessage(MESSAGE_TEXT_CHANGED, searchTerm), SEARCH_DELAY);
    }

    private void startSearch(final String searchQuery) {
        if (Util.isNetworkAvailable(SearchActivity.this)) {
            if (downloadManager != null && !downloadManager.isCancelled()) {
                downloadManager.cancel(true);
            }
            downloadManager = new DownloadManager(SearchActivity.this, searchQuery);
            downloadManager.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onPreExecute(String searchString) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPostExecute(String searchTerm, List<Page> pages) {
        Log.d(TAG, searchTerm);
        progressBar.setVisibility(View.GONE);
        searchFragment.updateSearchResults(searchTerm, pages);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_magnifying_glass:
                String searchTerm = searchBox.getText().toString().trim();
                if (searchTerm.length() >= MIN_CHARACTER_LENGTH) {
                    sendSearchMessage(searchTerm);
                }
                break;
        }
    }
}
