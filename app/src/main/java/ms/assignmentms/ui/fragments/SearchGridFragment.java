package ms.assignmentms.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import ms.assignmentms.R;
import ms.assignmentms.adapter.PageAdapter;
import ms.assignmentms.interfaces.SearchListener;
import ms.assignmentms.models.Page;
import ms.assignmentms.util.Util;

/**
 * Created by nbansal2211 on 06/06/16.
 */
public class SearchGridFragment extends Fragment implements SearchListener{

    private GridView gridView;
    private PageAdapter adapter;
    private List<Page> pageList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.layout_page_results, container, false);


        gridView = (GridView) rootView.findViewById(R.id.grid_pages);

        //Set number of columns for grid based on thumbnail size
        gridView.setNumColumns(Util.getNumberOfColumns(getActivity(), R.dimen.screen_padding));

        adapter = new PageAdapter(getActivity(), R.layout.row_item, pageList);
        gridView.setAdapter(adapter);
        return rootView;
    }




    public void updateSearchResults(String searchTerm, List<Page> pages) {
        this.pageList.clear();
        this.pageList.addAll(pages);
        adapter.notifyDataSetChanged();
    }
}
