package ms.assignmentms.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import ms.assignmentms.R;
import ms.assignmentms.adapter.PageAdapter;
import ms.assignmentms.interfaces.SearchListener;
import ms.assignmentms.models.Page;
import ms.assignmentms.util.Util;

/**
 * Created by nbansal2211 on 07/06/16.
 */
public class SearchListFragment extends Fragment implements SearchListener {

    private ListView listView;
    private PageAdapter adapter;
    private List<Page> pageList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);


        listView = (ListView) rootView.findViewById(R.id.listView);


        adapter = new PageAdapter(getActivity(), R.layout.row_list, pageList);
        listView.setAdapter(adapter);
        return rootView;
    }




    public void updateSearchResults(String searchTerm, List<Page> pages) {
        this.pageList.clear();
        this.pageList.addAll(pages);
        adapter.notifyDataSetChanged();
    }
}
