package ms.assignmentms.util;

/**
 * Created by nbansal2211 on 06/06/16.
 */
public interface AppConstants {
    String BASE_URL = "https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=thumbnail&pithumbsize=50&pilimit=100&generator=prefixsearch&gpssearch=%s";

    float THUMBNAIL_MAX_SIZE = 120.0f;
    interface REST_METHODS{
        String GET = "GET";
        String POST = "POST";
        String PUT = "PUT";
    }
    interface JSON_PARAMS{
        String QUERY = "query";
        String THUMBNAIL = "thumbnail";
        String PAGES = "pages";

        String PAGE_ID = "pageid";
        String PAGE_TITLE = "title";
        String PAGE_INDEX = "index";
        String SOURCE = "source";
        String WIDTH = "width";
        String HEIGHT = "height";
    }
}
