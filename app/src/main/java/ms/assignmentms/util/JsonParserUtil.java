package ms.assignmentms.util;

import android.text.TextUtils;
import android.webkit.URLUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ms.assignmentms.models.Thumbnail;
import ms.assignmentms.util.AppConstants.JSON_PARAMS;
import ms.assignmentms.models.Page;

/**
 * Created by nbansal2211 on 06/06/16.
 */
public class JsonParserUtil {


    public static synchronized List<Page> getPageList(String json) throws JSONException {

        List<Page> list = new ArrayList<>();
        if (!TextUtils.isEmpty(json)) {

            JSONObject rootObject = new JSONObject(json);

            if (rootObject != null && !rootObject.isNull(JSON_PARAMS.QUERY)) {

                JSONObject queryJsonObject = rootObject.optJSONObject(JSON_PARAMS.QUERY);
                if (!queryJsonObject.isNull(JSON_PARAMS.PAGES)) {
                    JSONObject pagesArray = queryJsonObject.optJSONObject(JSON_PARAMS.PAGES);
                    if (pagesArray != null) {
                        Iterator<String> pageIterator = pagesArray.keys();

                        while (pageIterator.hasNext()) {

                            JSONObject pageObject = pagesArray.optJSONObject(pageIterator.next());

                            if (pageObject != null && pageObject.has(JSON_PARAMS.THUMBNAIL)) {
                                Page page = new Page();
                                Thumbnail t = new Thumbnail();

                                page.setPageid(pageObject.optString(JSON_PARAMS.PAGE_ID));
                                page.setTitle(pageObject.optString(JSON_PARAMS.PAGE_TITLE));
                                page.setIndex(pageObject.optLong(JSON_PARAMS.PAGE_INDEX));

                                JSONObject thumbJson = pageObject.optJSONObject(JSON_PARAMS.THUMBNAIL);

                                t.setHeight(thumbJson.optInt(JSON_PARAMS.HEIGHT));
                                t.setWidth(thumbJson.optInt(JSON_PARAMS.WIDTH));
                                t.setSource(thumbJson.optString(JSON_PARAMS.SOURCE));

                                page.setThumbnail(t);

                                if (!TextUtils.isEmpty(t.getSource()) && URLUtil.isValidUrl(t.getSource()))
                                    list.add(page);
                            }
                        }
                    }
                }


            }

        }

        return list;
    }
}
