package ms.assignmentms.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.Display;

/**
 * Created by nbansal2211 on 06/06/16.
 */
public class Util {
    public static int getNumberOfColumns(Activity activity, int dimenPadding) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int padding =  (2 * activity.getResources().getDimensionPixelOffset(dimenPadding));
        int num = Math.round((size.x-padding) / AppConstants.THUMBNAIL_MAX_SIZE);
        Log.d("Util:", num + "");
        return num;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
